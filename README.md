# Summer School 2024


## Fast guide to use hypepy

- [ ] Download docker
- [ ] Import the image: hiperlife/hiperpy-arm64:beta for arm processors and hiperlife/hiperpy-amd64:beta for the rest.
- [ ] In the folder where you want to work, copy the docker-compose.yaml file, create a folder called "hiperpy-apps" inside the same folder where the yaml file is and run the following command:
```docker-compose up```
- [ ] Now you can access the jupyter hub.
- [ ] Also, you can open another terminal and run ```docker ps```, which will print the active images. Then, you can access the active containers using ```docker exec -it <container_name> bash```. This will open a terminal inside the container, where you can run python scripts, for example.
- [ ] The folder "/home/hiperpy/external/" of the container is shared between the container and the host. You can use it to store your data and scripts and then plot them using, for example, ParaView.


Note that if you use the terminal inside the container, you can use ```mpirun``` to run parallel simulations:

    ```mpirun -np 4 python script.py```