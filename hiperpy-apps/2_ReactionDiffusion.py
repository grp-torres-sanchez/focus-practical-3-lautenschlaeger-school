import hiperpy
import os

hiperpy.Init()

# Mesh creator. What are periodic boundary conditions?
mesh = hiperpy.Mesh()
mesh.setElemType("Triang")
mesh.setBasisFunctionType("Lagrangian")
mesh.setBasisFunctionOrder(1)
mesh.setPeriodicBoundaryCondition([hiperpy.Axis.Xaxis.value, hiperpy.Axis.Yaxis.value])
mesh.setGeometry("Square", 100, 10)
mesh.linkMeshGenerator()
mesh.setBalanceMesh(True)
mesh.Update()

# Field creator
field = hiperpy.Field(mesh)
field.setNameTag("mg")
field.setNumFields(2)
field.setFields(["u", "v"])
field.Update()

# Hiperproblem creator
hiperProblem = hiperpy.HiperProblem()
hiperProblem.setFields([field])
hiperProblem.setIntegration("IntegrationMorphogens", ["mg"])
hiperProblem.setCubatureGauss("IntegrationMorphogens", 3)
hiperProblem.defineIndices("IntegrationMorphogens", ["I", "J", "a"])

# Reaction-diffusion parameters. Try changing them!
hiperProblem.addParameter('dt', 0.1)
hiperProblem.addParameter('dc', 0.01)
hiperProblem.addParameter('dh', 0.1)
hiperProblem.addParameter('pc', 10.0)
hiperProblem.addParameter('ph', 5.0)

hiperProblem.activateModule("IntegrationMorphogens", "cartesian", options={"order": 1})


#Equations, write them!
hiperProblem.setRightHandSide("IntegrationMorphogens",
                              """
                                 Bk(I, 0) = ...;""")
hiperProblem.setMatrix("IntegrationMorphogens",
                       """
                             Ak(I, 0, J, 0) = ...;""")

hiperProblem.setRightHandSide("IntegrationMorphogens",
                              """
                                Bk(I, 1) = ...;""")

hiperProblem.setMatrix("IntegrationMorphogens",
                       """
                             Ak(I, 1, J, 1) = ...;""")

hiperProblem.Update()


# Initial conditions
field.setInitialCondition("c", "0.01*random", random="uniform")
field.setInitialCondition("h", "0.01*random", random="uniform")

field.printFileLegacyVtk(f"2DfieldMorphogens0")

# Solver
solver = hiperpy.Solver()
solver.setHiperProblem(hiperProblem)
solver.selectSolver("Aztec")
solver.setDefaultParameters()
#solver.setSequentialAnalysis()
solver.Update()

# Time loop
for i in range(1, 10000):
    # hiperProblem.fillLinearSystem()
    field.copyValueToPrevious()
    hiperProblem.UpdateGhosts()
    solver.solve()
    hiperProblem.UpdateSolution()
    print("Time", i)
    field.printFileVtk(f"./2DfieldMorphogens{i}", True,i,[],[])