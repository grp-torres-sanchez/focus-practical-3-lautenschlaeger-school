import hiperpy
import os

hiperpy.Init()

# Mesh creator. What are periodic boundary conditions?
mesh = hiperpy.Mesh("unstructured")
mesh.setElemType("Triang")
mesh.setBasisFunctionType("Lagrangian")
mesh.setBasisFunctionOrder(1)
mesh.setGeometry("Circle", 0.01)
mesh.linkMeshGenerator()
mesh.setBalanceMesh(True)
mesh.Update()

mesh.printFileLegacyVtk(f"mesh")


# Field creator
field = hiperpy.Field(mesh)
field.setNameTag("p")
field.setNumFields(2)
field.setFields(["px", "py"])
field.Update()


# Initial conditions
field.setInitialCondition("px","sqrt(x*x+y*y)<0.1 ? 0.001 * random : 0.0", random="uniform")
field.setBoundaryCondition("px", 0.0)
field.setBoundaryCondition("py", 0.0)
field.UpdateGhosts()


# Hiperproblem creator
hiperProblem = hiperpy.HiperProblem()
hiperProblem.setFields([field])
hiperProblem.setGlobalIntegrals(["Rayleighian"])
hiperProblem.setIntegration("Polar", ["polar"])
hiperProblem.setCubatureGauss("Polar", 3)

hiperProblem.defineIndices("Polar", ["N", "M", "I", "J", "a", "b", "c", "d", "i"])


# Reaction-diffusion parameters. Try changing them!
hiperProblem.addParameter('dt', 0.0001)
hiperProblem.addParameter('alpha', -10.0)
hiperProblem.addParameter('beta', 1.0)
hiperProblem.addParameter('lambda', 1.0)
hiperProblem.addParameter('gamma', 1.0)
hiperProblem.addParameter('L1', 0.1)
hiperProblem.addParameter('L2', 1.0)


# Usefull functions. What do they mean?
hiperProblem.activateModule("Polar", "cartesian", options={"order": 1})
hiperProblem.addDefinition("Polar", "scalar", "diff_norm_p2 = (polar_N-polar_tN0)*(polar_N-polar_tN0)")
hiperProblem.addDefinition("Polar", "scalar", "norm_p2 = polar_N*polar_N")
hiperProblem.addDefinition("Polar", "scalar", "norm_dp2 = dpolar_dx(a,b)*dpolar_dx(a,b)")
hiperProblem.addDefinition("Polar", "scalar", "divp = dpolar_dx(a,a)")

# Equations, write them!
hiperProblem.addDefinition("Polar", "scalar", "energy = ...")
hiperProblem.addDefinition("Polar", "scalar", "dissipation = ...")
hiperProblem.addDefinition("Polar", "scalar", "power = ...")
hiperProblem.setLagrangian("Polar", "lagrangian = energy + dissipation + power")
hiperProblem.Update()


# Solver
solver = hiperpy.Solver()
solver.setHiperProblem(hiperProblem)
solver.selectSolver("Mumps")
solver.setDefaultParameters()
solver.Update()

nonlinSolver = hiperpy.NonLinearSolver()
nonlinSolver.selectSolver("NewtonRaphson")
nonlinSolver.setLinearSolver(solver)
nonlinSolver.setMaxNumIterations(10)
nonlinSolver.setResTolerance(1.E-8)
nonlinSolver.setSolTolerance(1.E-8)
nonlinSolver.Update()


# Time loop
time = 0.0
maxTime = 50.0
deltat = 1.E-1
stepFactor = 1.1
n = 0
field.printFileVtk(f"sol{n}", True, time, [], [])

while time < maxTime:
    field.copyValueToPrevious()
    field.UpdateGhosts()
    deltat = float(hiperProblem.getParameter("dt"))
    if(hiperpy.MyRank()==0):
        print(n,time,deltat)

    # hiperProblem.fillLinearSystem()

    nonlinSolver.solve()

    if nonlinSolver.converged():
        n += 1
        time += deltat
        field.printFileVtk(f"sol{n}", True, time, [], [])

        if nonlinSolver.numberOfIterations() < 5:
            deltat *= stepFactor
    else:
        field.copyPreviousToCurrent()
        deltat /= stepFactor

    hiperProblem.updateParameter("dt", deltat)
hiperpy.Finalize()