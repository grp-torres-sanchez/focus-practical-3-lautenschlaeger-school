\documentclass[a4paper,12pt]{article}

\usepackage{ae} %??
\usepackage[utf8]{inputenc} % per a poder fer servir caràcters accentuats
\usepackage[english]{babel} % idioma per les coses automàtiques
\usepackage{amssymb}        % símbols de l'AMS
\usepackage{amsmath}        % macros de l'AMS
\usepackage[pdftex]{graphicx}  % poder incloure gràfics
\usepackage[pdftex]{color}     % poder fer servir color al text
\usepackage[T1]{fontenc}	% per a poder fer copypaste
\usepackage{hyperref}		% per a tenir hyperlinks
\usepackage{fancyhdr}		% per fer capçaleres i peus de pàgina més bonics

\usepackage{listings} % per a poder afegir codi
\usepackage{verbatim} % per a poder afegir codi sense formatar

\usepackage{tcolorbox} %Caixes de color
\newtcolorbox{caixa}{colback=black!5!white,colframe=black!5!white}

\usepackage{bbm}
\newcommand{\mbf}[1]{\mathbf{#1}}


\title{Focus practical 3: Finite elements for active matter: Turing patterns and Flocks}
\author{Torres-Sánchez Lab, EMBL Barcelona}
\date{July 2024}


\begin{document}
    \maketitle
    \section{Installation}

    First of all, we need to download and install Docker. Docker is a platform that allows us to run applications in isolated environments called containers. This way, we can ensure that the software runs in the same way on different computers. To install Docker, follow the instructions on the official website: \url{https://www.docker.com/products/docker-desktop/}.

    In order to run the software, we need to download the Docker image that contains all the necessary software. To do this, open a terminal and run the following command:
    \begin{verbatim}
        docker pull hiperlife/hiperpy-arm64:beta
    \end{verbatim}
    if you are using a Mac with an M chip, or
    \begin{verbatim}
        docker pull hiperlife/hiperpy-amd64:beta
    \end{verbatim}
    if you are using a Mac with an Intel chip, a Windows computer, or a Linux computer.

    Note that the Docker engine must be running in order to execute this command. If you are using a Mac or Windows computer, you can find the Docker Desktop application in the applications menu.

    In order to visualise the results, we will use the software Paraview. To install Paraview, follow the instructions on the official website: \url{https://www.paraview.org/download/}.

    \section{Running \texttt{hiperpy}}
    When running a Docker container by default, nothing is shared between the host computer and the container. This means that the files produced by our simulations will be stored inside the container and will be lost when the container is stopped. To avoid this, one can use a volume share between the host computer and the container.

    We provide a code template to run the Docker container that automatically shares the current directory with the container. Moreover, this command will also download the tutorials of \texttt{hiperpy} that we will use in this practical. This is the .yaml file in the folder we provided.

    To execute the code template, open a terminal, navigate to the directory where the file is located, and run the following command:
    \begin{verbatim}
        docker-compose up
    \end{verbatim}
    This command will start the Docker container and open a Jupyter notebook. To open the Jupyter notebook, copy the link that appears in the terminal and paste it into a web browser.

    \section{Theory}
    \subsection{Finite Element Method: Diffusion equation}
    First of all, we need to get familiarised with the Finite Element Method (FEM) that we will use to solve the equations of the system. %We provided notes on how the solve the Poisson equation using FEM. If you are not familiar with FEM, we recommend you to read the notes before starting the practical.
    We will begin to solve the following equation:
    \begin{equation}
        \nabla^2 u = f
    \end{equation}
    in a 2D square domain, $\Omega$.

    We also need to define the boundary conditions. There are two principal types of BCs that we will be working with. The first one is the Dirichlet boundary conditions. In this case, we impose the value of the solution at the boundary of the domain.

    The second type are Neumann boundary conditions. In this case we control the derivative of the solution. We impose that $\nabla u \cdot \vec n = h$. If we impose $\nabla u \cdot \vec n = 0$ then that boundary will be isolated, impermeable... (Why?) This is called the no flux condition or Neumann homogeneous BCs.

    We will call $\Gamma_D$ the part of the boundary where we impose Dirichlet BCs and $\Gamma_N$ the part of the boundary where we impose Neumann BCs. Thus, we have this boundary value problem:
    \begin{equation}
        \begin{cases}
            \nabla^2 u = f & \text{in } \Omega \\
            u = u_D & \text{on } \Gamma_D \\
            \nabla u \cdot \vec n = h & \text{on } \Gamma_N
        \end{cases}
    \end{equation}

    For solving this using FEM, we first need to write the equation in the week form. To do that we fist multiply it by a test function, $v\in H_0^1(\Omega)$, and we integrate it over the whole domain.

    $$
    \int_\Omega v \nabla^2 u d\Omega = \int_\Omega v f d\Omega
    $$

    integrating by parts on the left-hand side of the equation
    $$
        \int_\Omega v \Delta u d\Omega = \int_\Omega \nabla v \cdot \nabla u d\Omega - \int_{\partial \Omega} v \nabla u \cdot \vec n dS.
    $$
    Now we impose the boundary conditions. We use that $v=0$ at the Dirichlet Boundary. Hence,
    $$
        \int_\Omega \nabla v \nabla u d \Omega =  \int_\Omega v f d\Omega + \int_{\Gamma_N} v h dS
    $$
    From where we derive the Weak Form:
    \begin{caixa}

    Find $u\in H^1(\Omega)$ such that $u=u_D$ on $\Gamma_D$ and
    $$
        \int_\Omega \nabla v \nabla u d \Omega =  \int_\Omega v f d\Omega + \int_{\Gamma_N} v h dS
    $$
    for all $v\in H^1(\Omega)$ such that $v=0$ on $\Gamma_D$
    \end{caixa}

    We now have to discretise this equation using finite elements.

    In order to discretise the system, we impose that for the nodal points $\{x_i\}_{i=1,\dots,N}$ the functions $N_i(x)$ that are
    \begin{equation*}
        \begin{cases}
            N_i(x_j) = 1 &\text{if $i=j$}\\
            N_i(x_j) = 0 &\text{if $i\neq j$}
        \end{cases}
    \end{equation*}
    for each element. Then,
    $$
        u\approx u^h(x) = \sum_{i=0}^N u_i N_i(x)
    $$
    with $v=N_i$ (for $i\notin \Gamma_D)$ (Petrov–Galerkin method). Substituting in the weak form, we get:
    \begin{caixa}
        We have to solve the system $\mathbf{K} \vec u = \vec f$ with
        \begin{align}
            K_{i,j} &= \int_\Omega \nabla N_i \cdot \nabla N_j d\Omega \\
            f_i &= \int_\Omega N_i f d\Omega + \int_{\Gamma_N} N_i h dS \label{eq:sourceterm}
        \end{align}
        (before applying the Dirichlet Boundary conditions).
    \end{caixa}


    \subsection{Time integration}
    We now want to solve the equation
    \begin{equation}
        \partial_t u = \nabla^2 u
    \end{equation}
    on $\Omega$. For simplicity we will assume Dirichlet BCs. We can approximate the time as numere of steps, $n$, and the time step, $\Delta t$. We can then discretise the time derivative as:
    $$
        \partial_t u \approx \frac{u^{(n+1)}-u^(n)}{\Delta t}
    $$
    and thus the solution will now depend on the time such as $u(x,t) = u^{(n)} (x,t^{(n)})$ where $t^{(n)} = n \Delta t$.

    We can then write the weak form as
    $$
        \int_\Omega v \frac{u^{(n+1)}-u^{(n)}}{\Delta t} d\Omega = \int_\Omega \nabla v \nabla u^{(n+1)} d\Omega
    $$
    and discreising in space using FEM we get:
    \begin{equation}
        \mathbf{M} \vec u^{(n+1)}  - \Delta_t \mathbf{K} \vec u^{(n+1)} = \vec f
    \end{equation}
    where
    \begin{align}
        M_{i,j} &= \int_\Omega N_i N_j d\Omega \\
        K_{i,j} &=  \int_\Omega \nabla N_i \cdot \nabla N_j d\Omega \\
        f_i &= \int_\Omega N_i u^{(n)} d\Omega
    \end{align}
    Note that we have multiplied everything by $\Delta t$. This is to reduce the numerical error produced by division.




    \subsection{Patter formation}
    Now, we want to study patterning formation. In order to do that, we will use the Turin equations.

    We can write this equations as:
    \begin{align}
        \partial_t c - D_c \nabla^2 c &=  f_c(c,h) \\
        \partial_t h - D_h \nabla^2 h &=  f_v(c,h)
    \end{align}
    where $f_c$ and $f_h$ are the reaction functions.


    \subsection{Flocking}
    In this part we will use a different approach to model a biological system (which in fact is the same but written in a different way). Instead of writing the equations in terms of the concentration of the species we will write an energy function that will be minimised by the system.

    The good thing about hyperpy, is that we can provide a Lagrangian and the library will take care of the rest: differentiating, solving the equations, etc.

    Let $\vec v$ be the velocity of the particles, for example birds. We can write the energy the filed wants to minimise as:
    \begin{equation}
        \mathcal{F} [\vec v] = \int_\Omega \left( \frac{\alpha}2 | \vec v | ^2 + \frac{\beta}2 |\vec v|^4 + \frac{L_1}2 |\nabla \vec v_1| | \nabla \vec v_2 | + \frac{L_2}2 |\nabla \vec v|^2 \right) d\Omega
    \end{equation}

    Let's take a moment to understand the terms of the equation. The first two terms only depend on the velocity of the particles themselfs, thus will not be afected by the neighbourgs. This terms will force the particles to move at a certain speed. The third term will force the particles to align with their neighbours. The last term will make the particles avoid formating nodes. (Explicar millor)



    \section{Practical}

    \subsection{Diffusion equation}
    \begin{enumerate}
        \item Let us assume that $f=0$. We have provide the code for this solution, with some boundary conditions.
        \begin{itemize}
            \item Can you identify all the terms of the equations in the code?
            \item Change the size of the domain.
            \item Change the number of elements of the mesh to a coarse mesh. What is happening?
            \item Now, refine the mesh. What is happening?
        \end{itemize}
        \item Explore the different boundary conditions:
        \begin{itemize}
            \item Which BCs are we using?
            \item Change the Dirichlet BCs in the code for different functions. Are the results as you expect?
            \item How would you impose Neumann homogeneous BCs?
            \item Change the code to impose Neumann homogeneous BCs to all the borders. What is happening? Why?
            \item Play with different BCs.
        \end{itemize}
        \item Using equation (\ref{eq:sourceterm}) add the code needed to produce a constant source term.
        \item Now, we want to study the time evolution of the system. Add the code for the equations of the time discretisation. If you need it, we provide the code for the time intesrator (not the code to add to the equations!).
        \item Implement a force:
        $$
        \partial_t u = \nabla^2 u - \nabla \cdot (\vec F u) + f
        $$
        For example, a gravitational field. Study the steady state. Then, add a source term on the top of the domain. What is happening?
    \end{enumerate}

    \subsection{Pattern formation}
    \begin{enumerate}
        \item Justify the terms of the equations.
        \item Think about the reaction terms that we can use. A tool like desmos can be used to plot the functions.
        \item Do a linear stability analysis of the system to find the conditions for patterning formation.
        \item Implement the equations in the code. Use the previous time step to avoid having non linear terms. Note that we provide part of the code.
        \item Play with the parameters, specially the diffusion constant. Do you obtain patterning for all the values?
        \item What happens if we make the domain bigger?
        \item Think of a way to make the patterning scaling with the domain. Can we implement it?
    \end{enumerate}

    \subsection{Flocking}
    \begin{enumerate}
        \item Explain the energy function.
        \item Implement it in the code. Note that we provide all the code, only the energy functional is missing.
        \item Play with the parameters. What happens if we change the parameter related to advection?
    \end{enumerate}
\end{document}